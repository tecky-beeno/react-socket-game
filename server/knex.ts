import { config } from 'dotenv'
import Knex from 'knex'

config()

let mode = process.env.NODE_ENV || 'development'
let profile = require('./knexfile')[mode]

export let knex = Knex(profile)
