import { Knex } from 'knex'
import { logoApple, logoPaypal, logoYahoo, logoYoutube } from 'ionicons/icons'

export async function seed(knex: Knex): Promise<void> {
  // Deletes ALL existing entries
  await knex('user').del()

  // Inserts seed entries
  await knex('user').insert([
    { name: 'Alice', icon: logoApple },
    { name: 'Bob', icon: logoYoutube },
    { name: 'Charlie', icon: logoYahoo },
    { name: 'Dave', icon: logoPaypal },
  ])
}
