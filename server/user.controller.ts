import express, { Request, Response } from 'express'
import { ExpressController } from './express-controller'
import { UserService } from './user.service'

export class UserController extends ExpressController {
  constructor(private user: UserService) {
    super()
    this.router.get('/user', this.list)
  }

  list = (req: Request, res: Response) => {
    this.callAPI(req, res, () => this.user.list())
  }
}
