import { GameRoomService } from './game-room.service'
import { ExpressController } from './express-controller'
import express, { Request, Response } from 'express'
import { Server } from 'socket.io'
import { UserService } from './user.service'

export class GameRoomController extends ExpressController {
  constructor(
    private game: GameRoomService,
    private user: UserService,
    private io: Server,
  ) {
    super()
    this.router.post(
      '/game/room/:game_room_id',
      this.requireUser,
      this.joinRoom,
    )
    this.router.delete(
      '/game/room/:game_room_id',
      this.requireUser,
      this.leaveRoom,
    )
    this.router.post('/game/room', this.requireUser, this.createRoom)
    this.router.get('/game/room', this.list)
    this.io.on('connection', socket => {
      socket.on('/game/enter', () => {
        socket.join('/game/room')
      })
      socket.on('/game/leave', () => {
        socket.leave('/game/room')
      })
    })
  }

  list = async (req: Request, res: Response) => {
    return this.callAPI(req, res, () => this.game.list())
  }

  leaveRoom = async (req: Request, res: Response) => {
    const user_id = req.user_id
    const game_room_id = +req.params.game_room_id
    if (!game_room_id) {
      res.status(400).json({ error: 'missing game_room_id in req.params' })
      return
    }
    this.callAPI(req, res, async () => {
      await this.game.leaveRoom({ game_room_id, user_id })
      this.io.to('/game/room').emit('leaveRoom', { game_room_id, user_id })
      return {}
    })
  }

  joinRoom = async (req: Request, res: Response) => {
    const user_id = req.user_id
    const game_room_id = +req.params.game_room_id
    if (!game_room_id) {
      res.status(400).json({ error: 'missing game_room_id in req.params' })
      return
    }
    this.callAPI(req, res, async () => {
      let user = await this.user.getUserAvatar(user_id)
      await this.game.joinRoom({ game_room_id, user_id })
      this.io.to('/game/room').emit('joinRoom', { game_room_id, user })
      return {}
    })
  }

  createRoom = async (req: Request, res: Response) => {
    const user_id = req.user_id
    const { name } = req.body
    if (!name) {
      res.status(400).json({ error: 'missing name in req.body' })
      return
    }
    this.callAPI(req, res, async () => {
      let user = await this.user.getUserAvatar(user_id)
      let json = await this.game.createRoom(name, user_id)
      this.io
        .to('/game/room')
        .emit('newRoom', { name, id: json.game_room_id, user })
      return {}
    })
  }
}
