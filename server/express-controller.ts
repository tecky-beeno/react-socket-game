import express, { Request, Response, NextFunction } from 'express'
import { UserService } from './user.service'

declare global {
  namespace Express {
    interface Request {
      user_id: number
    }
  }
}

export class ExpressController {
  router = express.Router()

  callAPI = async (req: Request, res: Response, fn: () => any) => {
    try {
      let json = await fn()
      res.json(json)
    } catch (error) {
      res.status(500).json({ error: (error as Error).toString() })
    }
  }

  requireUser = (req: Request, res: Response, next: NextFunction) => {
    let user_id = +req.header('user-id')!
    if (!user_id) {
      res.status(401).json({ error: 'missing user-id in req.header' })
      return
    }
    req.user_id = user_id
    next()
  }
}
