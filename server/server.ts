import express from 'express'
import http from 'http'
import cors from 'cors'
import expressSession from 'express-session'
import * as listeningOn from 'listening-on'
import { join, resolve } from 'path'
import { config } from 'dotenv'
import { router } from './router'
import SocketIO from 'socket.io'
import { UserController } from './user.controller'
import { UserService } from './user.service'
import { knex } from './knex'
import { GameRoomService } from './game-room.service'
import { GameRoomController } from './game-room.controller'

config()

const app = express()
const server = http.createServer(app)
const io = new SocketIO.Server(server, {
  cors: {
    origin: '*',
  },
})

app.use(cors())

app.use(express.static('public'))

app.use(express.json() as any)
app.use(express.urlencoded({ extended: true }) as any)

const session = expressSession({
  secret: process.env.SESSION_SECRET || Math.random().toString(36),
  saveUninitialized: true,
  resave: true,
})
app.use(session)

app.use((req, res, next) => {
  if (req.method === 'GET') {
    console.log(req.method, req.url)
  } else {
    console.log(req.method, req.url, req.body)
  }
  next()
})

let userService = new UserService(knex)
let gameRoomService = new GameRoomService(knex)

let userController = new UserController(userService)
app.use(userController.router)

let gameRoomController = new GameRoomController(
  gameRoomService,
  userService,
  io,
)
app.use(gameRoomController.router)

app.use((req, res) => {
  // res.status(404).sendFile(resolve(join('public', '404.html')))
  res
    .status(404)
    .json({ error: `Route not found, Method: ${req.method}, Url: ${req.url}` })
})

const PORT = +process.env.PORT! || 8100

server.listen(PORT, () => {
  listeningOn.print(PORT)
})
