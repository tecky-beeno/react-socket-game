
import { Knex } from 'knex'

export async function up(knex: Knex): Promise<void> {

  if (!(await knex.schema.hasTable('user'))) {
    await knex.schema.createTable('user', table => {
      table.increments('id').primary()
      table.text('name').notNullable()
      table.text('icon').notNullable()
      table.timestamps(false, true)
    })
  }

  if (!(await knex.schema.hasTable('game_room'))) {
    await knex.schema.createTable('game_room', table => {
      table.increments('id').primary()
      table.text('name').notNullable()
      table.timestamps(false, true)
    })
  }

  if (!(await knex.schema.hasTable('game_room_member'))) {
    await knex.schema.createTable('game_room_member', table => {
      table.integer('user_id').notNullable().references('user.id')
      table.integer('game_room_id').notNullable().references('game_room.id')
      table.timestamps(false, true)
    })
  }

}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTableIfExists('game_room_member')
  await knex.schema.dropTableIfExists('game_room')
  await knex.schema.dropTableIfExists('user')
}

