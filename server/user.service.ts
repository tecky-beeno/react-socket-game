import { Knex } from 'knex'

export class UserService {
  constructor(private knex: Knex) {}
  list() {
    return this.knex.select('id', 'name').from('user')
  }
  getUserAvatar(id: number) {
    let user = this.knex
      .select('id', 'name', 'icon')
      .from('user')
      .where({ id })
      .first()
    if (!user) {
      throw new Error('User not found')
    }
    return user
  }
}
