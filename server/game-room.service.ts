import { Knex } from 'knex'

export class GameRoomService {
  constructor(private knex: Knex) {}

  async createRoom(name: string, user_id: number) {
    let [game_room_id] = await this.knex
      .insert({ name })
      .into('game_room')
      .returning('id')
    await this.joinRoom({ user_id, game_room_id: game_room_id as number })
    return { game_room_id }
  }

  joinRoom(row: { game_room_id: number; user_id: number }) {
    return this.knex.insert(row).into('game_room_member')
  }

  async leaveRoom(row: { game_room_id: number; user_id: number }) {
    await this.knex('game_room_member').where(row).delete()
  }

  async list() {
    let gameRoomList = await this.knex
      .select('game_room.id', 'game_room.name')
      .from('game_room')
      .innerJoin(
        'game_room_member',
        'game_room_member.game_room_id',
        'game_room.id',
      )
      .groupBy('game_room.id', 'game_room.name')
      .havingRaw('count(*) > 0')
    for (let gameRoom of gameRoomList) {
      let userList = await this.knex
        .select('user.id', 'user.name', 'user.icon')
        .from('game_room_member')
        .innerJoin('user', 'user.id', 'game_room_member.user_id')
      gameRoom.userList = userList
    }
    return gameRoomList
  }
}
