import {
  IonAvatar,
  IonButton,
  IonButtons,
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonCheckbox,
  IonCol,
  IonContent,
  IonHeader,
  IonInput,
  IonItem,
  IonLabel,
  IonList,
  IonListHeader,
  IonPage,
  IonRow,
  IonText,
  IonTitle,
  IonToolbar,
  useIonRouter,
  useIonToast,
} from '@ionic/react'
import { useEffect, useState } from 'react'
import { useStorageState } from 'react-use-storage-state'
import { del, post, useFetch } from '../hooks/use-fetch'
import { useSocket } from '../hooks/use-socket'
import './Tab1.css'

type User = {
  id: number
  name: string
}

type GameRoom = {
  id: number
  name: string
  userList: Avatar[]
}
type Avatar = { id: number; name: string; icon: string }

const GameTab: React.FC = () => {
  const [userID, setUserID] = useStorageState('user_id', 0)
  const [userList, setUserList] = useFetch<User[]>('/user', [])
  const [gameRoomList, setGameRoomList] = useFetch<GameRoom[]>('/game/room', [])
  const [newRoomName, setNewRoomName] = useState('')
  const [error, setError] = useState('')
  const router = useIonRouter()
  const [socket, useSocketEffect] = useSocket()
  const [present, dismiss] = useIonToast()

  useEffect(() => {
    if (error) {
      present({
        message: error,
        duration: 5 * 1000,
        position: 'bottom',
        color: 'danger',
      })
    }
  }, [error])

  useSocketEffect(() => {
    console.log('start socket effect')
    socket.emit('/game/enter')

    function onJoinRoom(event: { game_room_id: number; user: Avatar }) {
      console.log('onJoinRoom:', event)
      setGameRoomList(gameRoomList => {
        if ('error' in gameRoomList) {
          return gameRoomList
        }
        let room = gameRoomList.find(room => room.id === event.game_room_id)
        if (!room || room.userList.find(user => user.id === event.user.id)) {
          return gameRoomList
        }
        let newRoom = {
          ...room,
          userList: [...room.userList, event.user],
        }
        return gameRoomList.map(eachRoom => {
          if (eachRoom.id === newRoom.id) {
            return newRoom
          }
          return eachRoom
        })
      })
    }
    function onLeaveRoom(event: { game_room_id: number; user_id: number }) {
      console.log('onLeaveRoom:', event)
      setGameRoomList(gameRoomList => {
        if ('error' in gameRoomList) {
          return gameRoomList
        }
        let room = gameRoomList.find(room => room.id === event.game_room_id)
        if (!room) {
          return gameRoomList
        }
        let newRoom = {
          ...room,
          userList: room.userList.filter(user => user.id !== event.user_id),
        }
        return gameRoomList.map(eachRoom => {
          if (eachRoom.id === newRoom.id) {
            return newRoom
          }
          return eachRoom
        })
      })
    }
    function onNewRoom(event: { id: number; name: string; user: Avatar }) {
      console.log('onCreateRoom:', event)
      setGameRoomList(gameRoomList => {
        if ('error' in gameRoomList) {
          return gameRoomList
        }
        return [
          ...gameRoomList,
          {
            id: event.id,
            name: event.name,
            userList: [event.user],
          },
        ]
      })
    }

    socket.on('joinRoom', onJoinRoom)
    socket.on('leaveRoom', onLeaveRoom)
    socket.on('newRoom', onNewRoom)
    return () => {
      console.log('clear socket effect')
      socket.emit('/game/leave')
      socket.off('joinRoom', onJoinRoom)
      socket.off('leaveRoom', onLeaveRoom)
      socket.off('newRoom', onNewRoom)
    }
  })

  const hasJoin: boolean =
    'error' in gameRoomList
      ? false
      : !!gameRoomList.find(room =>
          room.userList.find(user => user.id === userID),
        )

  async function createRoom() {
    let json = await post('/game/room', { name: newRoomName })
    if (json.error) {
      setError(json.error)
      return
    }
    setError('')
    // router.push('/game/room/' + json.game_room_id)
  }

  async function joinRoom(id: number) {
    let json = await post('/game/room/' + id, {})
    if (json.error) {
      setError(json.error)
      return
    }
    setError('')
    // TODO check if enough people
    // router.push('/game/room/' + id)
  }

  async function leaveRoom(id: number) {
    let json = await del('/game/room/' + id)
    if (json.error) {
      setError(json.error)
      return
    }
    setError('')
  }

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Tab 1</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent className="ion-padding">
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Tab 1</IonTitle>
          </IonToolbar>
        </IonHeader>

        <IonList>
          <IonListHeader>Login User</IonListHeader>
          {'error' in userList
            ? null
            : userList.map(user => (
                <IonItem key={user.id}>
                  <IonLabel>
                    #{user.id} {user.name}
                  </IonLabel>
                  <IonCheckbox
                    checked={user.id === userID}
                    onIonChange={e => {
                      if (e.detail.checked) {
                        setUserID(user.id)
                      }
                    }}
                  ></IonCheckbox>
                </IonItem>
              ))}
        </IonList>

        <IonList>
          <IonListHeader>Create New Room</IonListHeader>
          <IonItem>
            <IonLabel>Room Name</IonLabel>
            <IonInput
              value={newRoomName}
              onIonChange={e => setNewRoomName(e.detail.value || '')}
            ></IonInput>
            <IonButton slot="end" onClick={createRoom}>
              Create
            </IonButton>
          </IonItem>
        </IonList>

        <IonList>
          <IonListHeader>Join Game Room</IonListHeader>
          {'error' in gameRoomList
            ? null
            : gameRoomList
                .filter(game => game.userList.length > 0)
                .map(room => {
                  let isInRoom = room.userList.find(user => user.id === userID)
                  return (
                    <IonCard key={room.id}>
                      <IonCardHeader>
                        <IonRow>
                          <IonCol>
                            #{room.id} {room.name} ({room.userList.length}/4)
                          </IonCol>
                          <IonCol size="auto">
                            <IonButtons>
                              {hasJoin && isInRoom ? (
                                <IonButton
                                  color="danger"
                                  onClick={() => leaveRoom(room.id)}
                                >
                                  Quit
                                </IonButton>
                              ) : (
                                <IonButton
                                  disabled={hasJoin}
                                  color="primary"
                                  onClick={() => joinRoom(room.id)}
                                >
                                  Join
                                </IonButton>
                              )}
                            </IonButtons>
                          </IonCol>
                        </IonRow>
                      </IonCardHeader>
                      <IonCardContent>
                        <div style={{ display: 'flex' }}>
                          {room.userList.map(user => (
                            <div key={user.id} className="ion-text-center">
                              <IonAvatar>
                                <img src={user.icon}></img>
                              </IonAvatar>
                              <IonText
                                color={user.id === userID ? 'primary' : ''}
                              >
                                {user.name}
                              </IonText>
                            </div>
                          ))}
                        </div>
                      </IonCardContent>
                    </IonCard>
                  )
                })}
        </IonList>
      </IonContent>
    </IonPage>
  )
}

export default GameTab
