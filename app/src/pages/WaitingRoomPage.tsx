import { useSocket } from '../hooks/use-socket'
import {
  IonBackButton,
  IonButtons,
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
} from '@ionic/react'
import { useParams } from 'react-router'

const WaitingRoomPage: React.FC = () => {
  const game_room_id = +useParams<{ game_room_id: string }>().game_room_id
  const socket = useSocket()
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonBackButton defaultHref="/tab/game"></IonBackButton>
          </IonButtons>
          <IonTitle>Waiting Room</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent className="ion-padding">todo</IonContent>
    </IonPage>
  )
}

export default WaitingRoomPage
