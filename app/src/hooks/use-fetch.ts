import { useEffect, useState } from 'react'

let server_origin = 'http://localhost:8200'

export async function del(url: string) {
  try {
    let res = await fetch(server_origin + url, {
      method: 'DELETE',
      headers: {
        'user-id': JSON.parse(localStorage.getItem('user_id')!),
      },
    })
    let json = await res.json()
    return json
  } catch (error) {
    return { error: (error as Error).toString() }
  }
}

export async function post(url: string, body: any) {
  try {
    let res = await fetch(server_origin + url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'user-id': JSON.parse(localStorage.getItem('user_id')!),
      },
      body: JSON.stringify(body),
    })
    let json = await res.json()
    return json
  } catch (error) {
    return { error: (error as Error).toString() }
  }
}

export function useFetch<T extends object>(url: string, defaultValue: T) {
  const [json, setJSON] = useState<T | { error: string }>(defaultValue)
  useEffect(() => {
    fetch(server_origin + url)
      .then(res => res.json())
      .then(setJSON)
      .catch(error => setJSON({ error: error.toString() }))
  }, [url])
  return [json, setJSON] as const
}
