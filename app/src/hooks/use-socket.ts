import { useEffect, useState } from 'react'
import { io, Socket } from 'socket.io-client'

let socket: Socket
let onSocketList: Array<() => void> = []

export function useSocket() {
  if (!socket) {
    socket = io('ws://localhost:8200')
    socket.on('connect', () => {
      console.log('connected socket.io')
      onSocketList.forEach(fn => fn())
    })
  }

  function useSocketEffect(onSocket: () => () => void) {
    useEffect(() => {
      onSocketList.push(onSocket)
      let offSocket = onSocket()
      return () => {
        offSocket()
        onSocketList = onSocketList.filter(fn => fn !== onSocket)
      }
    }, [socket])
  }

  return [socket, useSocketEffect] as const
}
