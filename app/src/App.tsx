import { Redirect, Route } from 'react-router-dom'
import {
  IonApp,
  IonIcon,
  IonLabel,
  IonRouterOutlet,
  IonTabBar,
  IonTabButton,
  IonTabs,
} from '@ionic/react'
import { IonReactRouter } from '@ionic/react-router'
import { cog, ellipse, settings, square, triangle } from 'ionicons/icons'
import GameTab from './pages/GameTab'
import Tab2 from './pages/Tab2'
import Tab3 from './pages/Tab3'

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css'

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css'
import '@ionic/react/css/structure.css'
import '@ionic/react/css/typography.css'

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css'
import '@ionic/react/css/float-elements.css'
import '@ionic/react/css/text-alignment.css'
import '@ionic/react/css/text-transformation.css'
import '@ionic/react/css/flex-utils.css'
import '@ionic/react/css/display.css'

/* Theme variables */
import './theme/variables.css'
import WaitingRoomPage from './pages/WaitingRoomPage'

const App: React.FC = () => (
  <IonApp>
    <IonReactRouter>
      <Route exact path="/">
        <Redirect to="/tab/game" />
      </Route>
      <Route exact path="/game/room/:game_room_id" component={WaitingRoomPage} />
      <Route path="/tab">
        <IonTabs>
          <IonRouterOutlet>
            <Route exact path="/tab/game">
              <GameTab />
            </Route>
            <Route exact path="/tab/market">
              <Tab2 />
            </Route>
            <Route path="/tab/photo">
              <Tab3 />
            </Route>
            <Route path="/tab/settings">
              <Tab3 />
            </Route>
          </IonRouterOutlet>
          <IonTabBar slot="bottom">
            <IonTabButton tab="tab-game" href="/tab/game">
              <IonIcon icon={triangle} />
              <IonLabel>Board Game</IonLabel>
            </IonTabButton>
            <IonTabButton tab="tab-market" href="/tab/market">
              <IonIcon icon={ellipse} />
              <IonLabel>Market</IonLabel>
            </IonTabButton>
            <IonTabButton tab="tab-photo" href="/tab/photo">
              <IonIcon icon={square} />
              <IonLabel>Photo Feed</IonLabel>
            </IonTabButton>
            <IonTabButton tab="tab-settings" href="/tab/settings">
              <IonIcon md={settings} ios={cog} />
              <IonLabel>Settings</IonLabel>
            </IonTabButton>
          </IonTabBar>
        </IonTabs>
      </Route>
    </IonReactRouter>
  </IonApp>
)

export default App
